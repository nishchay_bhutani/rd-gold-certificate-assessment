package com.epam.assessment.dto;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BatchDTO {
	private int id;
	@NotNull
	private String name;
	@NotNull
	private String practice;
	@NotNull
	@DateTimeFormat
	private LocalDate startDate;
	@NotNull
	@DateTimeFormat
	private LocalDate endDate;
}
