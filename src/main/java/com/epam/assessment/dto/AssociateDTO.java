package com.epam.assessment.dto;


import com.epam.assessment.dao.Batch;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AssociateDTO {
	private int id;
	@NotNull
	private String name;
	@NotNull
	@Email
	private String email;
	@NotNull
	private String gender;
	@NotNull
	private String college;
	@NotNull
	private String status;
	@NotNull
	private Batch batch;
}
