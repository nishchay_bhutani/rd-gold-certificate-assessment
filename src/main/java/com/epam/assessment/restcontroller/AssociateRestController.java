package com.epam.assessment.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.assessment.dto.AssociateDTO;
import com.epam.assessment.service.AssociateService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@RestController
@RequestMapping("/rd/associates")
public class AssociateRestController {
	@Autowired
	AssociateService associateService;

	@PostMapping
	public ResponseEntity<AssociateDTO> addAssociate(@RequestBody @Valid AssociateDTO associateDTO) {
		return new ResponseEntity<>(associateService.add(associateDTO), HttpStatus.CREATED);
	}

	@GetMapping("{gender}")
	public ResponseEntity<List<AssociateDTO>> getAssociateByGender(@PathVariable @Valid @NotBlank String gender) {
		return new ResponseEntity<>(associateService.getByGender(gender), HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<AssociateDTO> updateAssociate(@RequestBody @Valid AssociateDTO associateDTO) {
		return new ResponseEntity<>(associateService.update(associateDTO), HttpStatus.OK);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<Void> deleteAssociate(@PathVariable @Valid @NotNull int id) {
		associateService.delete(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
