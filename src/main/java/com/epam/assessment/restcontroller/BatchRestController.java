package com.epam.assessment.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.assessment.dto.BatchDTO;
import com.epam.assessment.service.BatchService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/rd/batches")
public class BatchRestController {

	@Autowired
	BatchService batchService;

	@PostMapping
	public ResponseEntity<BatchDTO> addBatch(@RequestBody @Valid BatchDTO batchDTO) {
		return new ResponseEntity<>(batchService.add(batchDTO), HttpStatus.CREATED);
	}

}
