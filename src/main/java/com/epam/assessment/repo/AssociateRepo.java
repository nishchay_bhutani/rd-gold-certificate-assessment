package com.epam.assessment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.assessment.dao.Associate;

public interface AssociateRepo extends JpaRepository<Associate, Integer>{
	public List<Associate> getByGender(String gender);
}
