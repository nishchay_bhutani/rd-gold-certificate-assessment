package com.epam.assessment.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.assessment.dao.Batch;

public interface BatchRepo extends JpaRepository<Batch, Integer>{
	
}
