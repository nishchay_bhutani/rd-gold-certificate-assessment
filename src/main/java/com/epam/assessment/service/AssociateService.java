package com.epam.assessment.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.assessment.dao.Associate;
import com.epam.assessment.dao.Batch;
import com.epam.assessment.dto.AssociateDTO;
import com.epam.assessment.exception.AssociateException;
import com.epam.assessment.repo.AssociateRepo;
import com.epam.assessment.repo.BatchRepo;

@Service
public class AssociateService {
	@Autowired
	AssociateRepo associateRepo;

	@Autowired
	BatchRepo batchRepo;

	@Autowired
	ModelMapper modelMapper;

	private static final String NOT_FOUND = "associate not found";

	public AssociateDTO add(AssociateDTO associateDTO) {
		Batch batch = batchRepo.findById(associateDTO.getBatch().getId())
				.orElseThrow(() -> new AssociateException("Invalid batch id."));
		if (!batch.equals(associateDTO.getBatch())) {
			throw new AssociateException("Invalid batch details.");
		}
		Associate associate = modelMapper.map(associateDTO, Associate.class);
		return modelMapper.map(associateRepo.save(associate), AssociateDTO.class);
	}

	public List<AssociateDTO> getByGender(String gender) {
		return associateRepo.getByGender(gender).stream()
				.map(associate -> modelMapper.map(associate, AssociateDTO.class)).toList();
	}

	public AssociateDTO update(AssociateDTO associateDTO) {
		if (!associateRepo.existsById(associateDTO.getId())) {
			throw new AssociateException(NOT_FOUND);
		}
		Associate associate = modelMapper.map(associateDTO, Associate.class);
		return modelMapper.map(associate, AssociateDTO.class);
	}

	public void delete(int associateId) {
		if (!associateRepo.existsById(associateId)) {
			throw new AssociateException(NOT_FOUND);
		}
		associateRepo.deleteById(associateId);
	}

}
