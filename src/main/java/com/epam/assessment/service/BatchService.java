package com.epam.assessment.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.assessment.dao.Batch;
import com.epam.assessment.dto.BatchDTO;
import com.epam.assessment.repo.BatchRepo;

@Service
public class BatchService {

	@Autowired
	BatchRepo batchRepo;

	@Autowired
	ModelMapper modelMapper;

	public BatchDTO add(BatchDTO batchDTO) {
		Batch batch = modelMapper.map(batchDTO, Batch.class);
		return modelMapper.map(batchRepo.save(batch), BatchDTO.class);
	}
}
