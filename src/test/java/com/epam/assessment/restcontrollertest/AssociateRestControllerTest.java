package com.epam.assessment.restcontrollertest;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.assessment.dao.Associate;
import com.epam.assessment.dao.Batch;
import com.epam.assessment.dto.AssociateDTO;
import com.epam.assessment.exception.AssociateException;
import com.epam.assessment.restcontroller.AssociateRestController;
import com.epam.assessment.service.AssociateService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(AssociateRestController.class)
public class AssociateRestControllerTest {
	@Autowired
	MockMvc mockMvc;

	@MockBean
	AssociateService associateService;

	Associate associate;
	AssociateDTO associateDTO;

	@BeforeEach
	void setUp() {
		associate = new Associate();
		associate.setName("Nishchay");
		associate.setEmail("nishchay@email.com");
		associate.setGender("M");
		associate.setStatus("a");
		associate.setCollege("Chitkara University");
		associate.setBatch(new Batch());
		associateDTO = new AssociateDTO();
		associateDTO.setName("Nishchay");
		associateDTO.setEmail("nishchay@email.com");
		associateDTO.setGender("M");
		associateDTO.setStatus("a");
		associateDTO.setCollege("Chitkara University");
		associateDTO.setBatch(new Batch());
	}

	@Test
	void testAddAssociate() throws Exception {
		when(associateService.add(associateDTO)).thenReturn(associateDTO);
		mockMvc.perform(post("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isCreated()).andExpect(jsonPath("$.name", is(associateDTO.getName())));
		verify(associateService).add(associateDTO);
	}

	@Test
	void testUpdateAssociate() throws Exception {
		when(associateService.update(associateDTO)).thenReturn(associateDTO);
		mockMvc.perform(put("/rd/associates").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isOk()).andExpect(jsonPath("$.name", is(associateDTO.getName())));
		verify(associateService).update(associateDTO);
	}

	@Test
	void testGetAssociateByGender() throws Exception {
		List<AssociateDTO> associateList = List.of(new AssociateDTO());
		when(associateService.getByGender(anyString())).thenReturn(associateList);
		mockMvc.perform(get("/rd/associates/{gender}", "M")).andExpect(status().isOk())
				.andExpect(jsonPath("$.size()", is(associateList.size())));
		verify(associateService).getByGender("M");
	}

	@Test
	void testDeleteAssociate() throws Exception {
		doNothing().when(associateService).delete(anyInt());
		mockMvc.perform(delete("/rd/associates/{id}", 5)).andExpect(status().isNoContent());
		verify(associateService).delete(anyInt());
	}

	@Test
	void testGetAssociateByGenderThrowsDataIntegrityViolationException() throws Exception {
	when(associateService.getByGender("M")).thenThrow(new DataIntegrityViolationException("Data Integrity exception"));
	mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
	.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());
	}

	@Test
	void testGetAssociateByGenderThrowsAssociateException() throws Exception {
	when(associateService.getByGender("M")).thenThrow(new AssociateException("Associate exception"));
	mockMvc.perform(get("/rd/associates/{gender}","M").contentType(MediaType.APPLICATION_JSON)
	.content(new ObjectMapper().writeValueAsString(associateDTO))).andExpect(status().isBadRequest());
	}

}
