//package com.epam.assessment.restcontrollertest;
//
//import static org.hamcrest.CoreMatchers.is;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import java.time.LocalDate;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//
//import com.epam.assessment.dao.Batch;
//import com.epam.assessment.dto.BatchDTO;
//import com.epam.assessment.restcontroller.BatchRestController;
//import com.epam.assessment.service.BatchService;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//
//@WebMvcTest(BatchRestController.class)
//public class BatchControllerTest {
//	
//	@MockBean
//	BatchService batchService;
//
//	@Autowired
//	MockMvc mockMvc;
//
//	Batch batch;
//	BatchDTO batchDTO;
//	@BeforeEach
//	void setUp() {
//		batch = new Batch();
//		batch.setName("RD");
//		batch.setPractice("Java");
//		batch.setStartDate(LocalDate.now());
//		batch.setEndDate(LocalDate.now());
//		batchDTO = new BatchDTO();
//		batchDTO.setName("RD");
//		batchDTO.setPractice("Java");
//		batchDTO.setStartDate(LocalDate.now());
//		batchDTO.setEndDate(LocalDate.now());
//	}
//	
//	@Test
//	void testAddBatch() throws Exception {
//		when(batchService.add(batchDTO)).thenReturn(batchDTO);
//		mockMvc.perform(post("/batches").contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(batchDTO))).andExpect(status().isCreated()).andExpect(jsonPath("$.name", is(batchDTO.getName())));
//	}
//
//}
