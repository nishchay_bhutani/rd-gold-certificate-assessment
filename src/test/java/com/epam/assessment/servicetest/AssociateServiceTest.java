package com.epam.assessment.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.epam.assessment.dao.Associate;
import com.epam.assessment.dao.Batch;
import com.epam.assessment.dto.AssociateDTO;
import com.epam.assessment.exception.AssociateException;
import com.epam.assessment.repo.AssociateRepo;
import com.epam.assessment.repo.BatchRepo;
import com.epam.assessment.service.AssociateService;

@SpringBootTest
public class AssociateServiceTest {
	
	@MockBean
	AssociateRepo associateRepo;
	
	@MockBean
	BatchRepo batchRepo;
	
	@MockBean
	ModelMapper modelMapper;
	
	@Autowired
	AssociateService associateService;
	
	Associate associate;
	AssociateDTO associateDTO;
	@BeforeEach
	void setUp() {
		associate = new Associate();
		associate.setName("Nishchay");
		associate.setEmail("nishchay@email.com");
		associate.setGender("M");
		associate.setStatus("a");
		associate.setCollege("Chitkara University");
		associate.setBatch(new Batch());
		associateDTO = new AssociateDTO();
		associateDTO.setName("Nishchay");
		associateDTO.setEmail("nishchay@email.com");
		associateDTO.setGender("M");
		associateDTO.setStatus("a");
		associateDTO.setCollege("Chitkara University");
		associateDTO.setBatch(new Batch());
		
		associate.getBatch();
		associate.getCollege();
		associate.getEmail();
		associate.getGender();
		associate.getStatus();
	}
	
	@Test
	void testAddAssociate() {
		when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		when(associateRepo.save(associate)).thenReturn(associate);
		when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		when(batchRepo.findById(anyInt())).thenReturn(Optional.of(associate.getBatch()));
		AssociateDTO result = associateService.add(associateDTO);
		assertEquals(result, associateDTO);
		verify(modelMapper).map(associateDTO, Associate.class);
		verify(modelMapper).map(associate, AssociateDTO.class);
		verify(associateRepo).save(associate);
		verify(batchRepo).findById(anyInt());
	}
	
	@Test
	void testAddAssociateIncorrectBatchIdFail() {
		when(batchRepo.findById(anyInt())).thenReturn(Optional.empty());
		assertThrows(AssociateException.class, () -> associateService.add(associateDTO));
		verify(batchRepo).findById(anyInt());
	}
	
	@Test 
	void testAddAssociateIncorrectBatchDetailsFail() {
		Batch randomBatch = new Batch();
		randomBatch.setName("rand");
		when(batchRepo.findById(anyInt())).thenReturn(Optional.of(randomBatch));
		assertThrows(AssociateException.class, () -> associateService.add(associateDTO));
		verify(batchRepo).findById(anyInt());
	}
	
	@Test
	void testGetAssociatesByGender() {
		List<Associate> associateList = List.of(associate);
		List<AssociateDTO> associateDtoList = List.of(associateDTO);
		when(associateRepo.getByGender(anyString())).thenReturn(associateList);
		when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		List<AssociateDTO> result = associateService.getByGender("M");
		assertEquals(result, associateDtoList);
		verify(associateRepo).getByGender("M");
		verify(modelMapper).map(associate, AssociateDTO.class);
	}
	
	@Test
	void testUpdateAssociate() {
		when(associateRepo.existsById(anyInt())).thenReturn(true);
		when(modelMapper.map(associateDTO, Associate.class)).thenReturn(associate);
		when(modelMapper.map(associate, AssociateDTO.class)).thenReturn(associateDTO);
		AssociateDTO result = associateService.update(associateDTO);
		assertEquals(result, associateDTO);
		verify(associateRepo).existsById(anyInt());
		verify(modelMapper).map(associateDTO, Associate.class);
		verify(modelMapper).map(associate, AssociateDTO.class);
	}
	
	@Test
	void testUpdateAssociateIncorrectIdFail() {
		when(associateRepo.existsById(anyInt())).thenReturn(false);
		assertThrows(AssociateException.class, () -> associateService.update(associateDTO));
		verify(associateRepo).existsById(anyInt());
	}
	
	@Test
	void testDeleteAssociate() {
		when(associateRepo.existsById(anyInt())).thenReturn(true);
		doNothing().when(associateRepo).deleteById(anyInt());
		associateService.delete(5);
		verify(associateRepo).deleteById(5);
		verify(associateRepo).existsById(5);
	}
	
	@Test
	void testDeleteAssociateIncorrectIdFail() {
		when(associateRepo.existsById(anyInt())).thenReturn(false);
		assertThrows(AssociateException.class, () -> associateService.delete(5));
		verify(associateRepo).existsById(5);
	}
	
}
