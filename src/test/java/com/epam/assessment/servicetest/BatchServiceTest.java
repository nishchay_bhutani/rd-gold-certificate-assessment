package com.epam.assessment.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.epam.assessment.dao.Batch;
import com.epam.assessment.dto.BatchDTO;
import com.epam.assessment.repo.BatchRepo;
import com.epam.assessment.service.BatchService;

@SpringBootTest
public class BatchServiceTest {
	@MockBean
	BatchRepo batchRepo;
	
	@MockBean
	ModelMapper modelMapper;
	
	@Autowired
	BatchService batchService;
	
	Batch batch;
	BatchDTO batchDTO;
	@BeforeEach
	void setUp() {
		batch = new Batch();
		batch.setName("RD");
		batch.setPractice("Java");
		batch.setStartDate(LocalDate.now());
		batch.setEndDate(LocalDate.now());
		batchDTO = new BatchDTO();
		batchDTO.setName("RD");
		batchDTO.setPractice("Java");
		batchDTO.setStartDate(LocalDate.now());
		batchDTO.setEndDate(LocalDate.now());
		
		batch.getName();
		batch.getPractice();
		batch.getStartDate();
		batch.getEndDate();
		batch.getId();
	}
	
	@Test
	void testAddBatch() {
		when(modelMapper.map(batchDTO, Batch.class)).thenReturn(batch);
		when(batchRepo.save(batch)).thenReturn(batch);
		when(modelMapper.map(batch, BatchDTO.class)).thenReturn(batchDTO);
		BatchDTO result = batchService.add(batchDTO);
		assertEquals(result, batchDTO);
		verify(modelMapper).map(batchDTO, Batch.class);
		verify(batchRepo).save(batch);
		verify(modelMapper).map(batch, BatchDTO.class);
	}
}
